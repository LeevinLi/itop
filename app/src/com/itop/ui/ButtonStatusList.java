package com.itop.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.itop.R;

/**
 * Created by Leevin on 2014/5/16 0016.
 */
public class ButtonStatusList extends Activity implements AdapterView.OnItemClickListener {

    //----------------------------comp
    private ListView lv_button_status;

    //---------------------------extra
    public static final int TYPE_1 = 1;
    public static final int TYPE_2 = 2;
    public static final int TYPE_3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button_status_list);
        initViews();
        initData();
        initListener();
    }

    private void initData() {
        lv_button_status.setAdapter(new ButtonAdapter(ButtonStatusList.this, getData()));
    }

    private void initListener() {
        lv_button_status.setOnItemClickListener(this);
    }

    private void initViews() {
        lv_button_status = (ListView) findViewById(R.id.lv_button_status);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private List<Item> getData() {
        List<Item> dataList = new ArrayList<Item>();
        for (int i = 1;i < 50;i++){
            Item item = new Item();
            item.setId(i);
            if(i % 2 == 0){
                item.setType(TYPE_2);
            }else if(i % 3 == 0){
                item.setType(TYPE_3);
            }else{
                item.setType(TYPE_1);
            }
            item.setContent("Button_"+i);
            dataList.add(item);
        }
        return dataList;
    }

    ;

    private class ButtonAdapter extends BaseAdapter {

        private Context context;
        private List<Item> dataList;

        private ButtonAdapter(Context context, List<Item> data) {
            this.context = context;
            this.dataList = data;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Object getItem(int position) {
            return dataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            HolderView holderView;
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                holderView = new HolderView();
                convertView = inflater.from(context).inflate(R.layout.list_item, null);
                holderView.itemBtn = (Button) convertView.findViewById(R.id.btn_item);
                convertView.setTag(holderView);
            } else {
                holderView = (HolderView) convertView.getTag();
            }
            //convertView = inflater.from(context).inflate(R.layout.list_item, null);
            //Button statusBtn = (Button) convertView.findViewById(R.id.btn_item);
            Item item = dataList.get(position);

            switch (item.getType()) {
                case TYPE_1:
                    holderView.itemBtn.setVisibility(View.VISIBLE);
                    holderView.itemBtn.setText(item.content + "one");
                    holderView.itemBtn.setBackgroundColor(getResources().getColor(android.R.color.holo_orange_light));
                    break;
                case TYPE_2:
                    holderView.itemBtn.setVisibility(View.VISIBLE);
                    holderView.itemBtn.setText(item.content + "twe");
                    holderView.itemBtn.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                    break;
                case TYPE_3:
                    holderView.itemBtn.setVisibility(View.GONE);
                    holderView.itemBtn.setText(item.content + "three");
                    break;
            }
            return convertView;
        }

        class HolderView {
            Button itemBtn;
        }
    }

    ;

    private class Item {
        private int id;
        private int type;
        private String content;

        private Item() {
        }

        private Item(int id, int type, String content) {
            this.id = id;
            this.type = type;
            this.content = content;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
