package com.itop.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.itop.R;
import com.itop.fragment.DialogView;

public class Main extends Activity implements View.OnClickListener {
	private Button cacheButtonStatus, dialogFragmentBtn, weatherBtn;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initViews();
		initListener();
	}

	private void initListener() {
		cacheButtonStatus.setOnClickListener(this);
		dialogFragmentBtn.setOnClickListener(this);
		weatherBtn.setOnClickListener(this);

	}

	private void initViews() {
		cacheButtonStatus = (Button) findViewById(R.id.cacheBtn);
		dialogFragmentBtn = (Button) findViewById(R.id.dialogFragmentBtn);
		weatherBtn = (Button) findViewById(R.id.weatherBtn);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.cacheBtn) {
			startActivity(new Intent(Main.this, ButtonStatusList.class));
		} else if (v.getId() == R.id.dialogFragmentBtn) {
			DialogView dialogView = DialogView.Instance("TITLE", "Message", new DialogView.NoticeDialogListener() {
				@Override
				public void onDialogPositiveClick() {
					Toast.makeText(Main.this, "PositiveClick", Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onDialogNegativeClick() {
					Toast.makeText(Main.this, "NegativeClick", Toast.LENGTH_SHORT).show();
				}
			});
			dialogView.show(getFragmentManager(), "DialogView");
		} else if (v.getId() == R.id.weatherBtn) {
		}
	}
}
