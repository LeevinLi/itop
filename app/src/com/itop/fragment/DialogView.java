package com.itop.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;

public class DialogView extends DialogFragment {

	// 使用这个接口的实例提供行动的事件
	static NoticeDialogListener mListener;

	/**
	 * 实现这个接口的类需要实现这两个方法
	 */
	public interface NoticeDialogListener {
		public void onDialogPositiveClick();

		public void onDialogNegativeClick();
	}

	public static DialogView Instance(String title, String message, NoticeDialogListener listener) {
		DialogView mDialogView = new DialogView();
		Bundle bundle = new Bundle();
		bundle.putString("title", title);
		bundle.putString("message", message);
		mDialogView.setArguments(bundle);
		mListener = listener;
		return mDialogView;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		String message = getArguments().getString("message");
		ContextThemeWrapper theme = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
		AlertDialog.Builder builder = new AlertDialog.Builder(theme);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dismiss();
				if (mListener != null) {
					mListener.onDialogPositiveClick();
				}
			}
		});
		builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dismiss();
				if (mListener != null) {
					mListener.onDialogNegativeClick();
				}
			}
		});
		return builder.create();
	}
}
