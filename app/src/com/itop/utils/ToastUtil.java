/**
 * 
 */
package com.itop.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * ToastUtil.java Created by Leevin on 2014年6月11日
 */
public class ToastUtil {

	/**
	 * 显示Toast
	 * 
	 * @param context
	 * @param resouce
	 *            资源ID
	 */
	public static void showToastShort(Context context, int resouce) {
		showToast(context, context.getResources().getString(resouce), Toast.LENGTH_SHORT);
	}

	/**
	 * 显示Toast
	 * 
	 * @param context
	 * @param msg
	 *            显示信息
	 */
	public static void showToastShort(Context context, String msg) {
		showToast(context, msg, Toast.LENGTH_SHORT);
	}

	/**
	 * 显示Toast
	 * 
	 * @param context
	 * @param resouce
	 *            资源ID
	 */
	public static void showToastLong(Context context, int resouce) {
		showToast(context, context.getResources().getString(resouce), Toast.LENGTH_LONG);
	}

	/**
	 * 显示Toast
	 * 
	 * @param context
	 * @param msg
	 *            显示信息
	 */
	public static void showToastLong(Context context, String msg) {
		showToast(context, msg, Toast.LENGTH_LONG);
	}

	private static void showToast(Context context, String msg, int duration) {
		Toast.makeText(context, msg, duration).show();
	}
}
