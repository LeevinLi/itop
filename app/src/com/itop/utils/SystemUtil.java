package com.itop.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * 系统工具类
 * 
 * @author Jin.Li
 * @date 2014年10月11日
 */
public class SystemUtil {

	private static final String TAG = SystemUtil.class.getSimpleName();

	/**
	 * 获取屏幕分辨率
	 * 
	 * @param context
	 * @return
	 */
	public static float getScreenDensity(Context context) {
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics dm = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(dm);
		return dm.density;
	}

	/**
	 * 获取设备屏幕宽度
	 * 
	 * @param context
	 * @return
	 */
	public static int getSreenWidth(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		return displayMetrics.widthPixels;
	}

	/**
	 * 获取设备屏幕高度
	 * 
	 * @param context
	 * @return
	 */
	public static int getSreenHeight(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		return displayMetrics.heightPixels;
	}

	/**
	 * dipToPx
	 * 
	 * @param context
	 * @param i
	 * @return
	 */
	public static int dipToPx(Context context, int dipValue) {
		float f = context.getResources().getDisplayMetrics().density;
		return (int) ((float) dipValue * f + 0.5F);
	}

	/**
	 * px与dip转换
	 * 
	 * @param context
	 * @param pxValue
	 * @return
	 */
	public static int pxTodip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * 获取装置的IEMI
	 * 
	 * @param context
	 * @return
	 */
	public static String getIMEI(Context context) {
		String IMEI = null;
		try {
			IMEI = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		} catch (Exception ex) {
			LogUtil.d(TAG, "AttriExtractor IMEI : " + ex.toString());
		}
		return TextUtils.isEmpty(IMEI) ? "Not Found IMEI" : IMEI;
	}

	/**
	 * 获取装置的IMSI
	 * 
	 * @param context
	 * @return
	 */
	public static String getIMSI(Context context) {
		String IMSI = null;
		try {
			IMSI = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getSubscriberId();
		} catch (Exception ex) {
			LogUtil.d(TAG, "getIMSI : " + ex.toString());
		}
		return TextUtils.isEmpty(IMSI) ? "Not Found IMSI" : IMSI;
	}

	/**
	 * 获取当前系统版本Code
	 * 
	 * @param context
	 * @return
	 */
	public static int getVersionCode(Context context) {
		int versionCode = -1;
		try {
			PackageInfo packInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			versionCode = packInfo.versionCode;
		} catch (Exception ex) {
			LogUtil.d(TAG, "getVersionCode : " + ex.toString());
		}
		return versionCode <= 0 ? -1 : versionCode;
	}

	/**
	 * 获取当前系统版本Name
	 * 
	 * @param context
	 * @return
	 */
	public static String getVersionName(Context context) {
		String versionName = "";
		try {
			PackageInfo packInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			versionName = packInfo.versionName;
		} catch (Exception ex) {
			LogUtil.d(TAG, "getVersionName : " + ex.toString());
		}
		return TextUtils.isEmpty(versionName) ? "Not Found VersionName" : versionName;
	}

	/**
	 * 获取设备系统版本信息
	 * 
	 * @return
	 */
	public static String getOSVersionInfo() {
		/*
		 * return "device: "+android.os.Build.DEVICE + ",product:" +
		 * android.os.Build.PRODUCT + ",manufacturer: " +
		 * android.os.Build.MANUFACTURER + ",model: " + android.os.Build.MODEL;
		 */
		// return android.os.Build.VERSION.SDK+"-"+android.os.Build.MANUFACTURER
		// + "-"+ android.os.Build.PRODUCT + ("".equals(CUSTOM_TAG) ? "" : "-" +
		// CUSTOM_TAG);
		return android.os.Build.VERSION.RELEASE;
	}

	/**
	 *  当前使用的网络类型： 
	 * 
	 *  例如： NETWORK_TYPE_UNKNOWN 网络类型未知 0 
	 * 
	 * NETWORK_TYPE_GPRS GPRS网络 1 
	 * 
	 * NETWORK_TYPE_EDGE EDGE网络 2 
	 * 
	 * NETWORK_TYPE_UMTS UMTS网络 3 
	 * 
	 * NETWORK_TYPE_HSDPA HSDPA网络 8 
	 * 
	 * NETWORK_TYPE_HSUPA HSUPA网络 9 
	 * 
	 * NETWORK_TYPE_HSPA HSPA网络 10 
	 * 
	 * NETWORK_TYPE_CDMA CDMA网络,IS95A 或 IS95B. 4 
	 * 
	 * NETWORK_TYPE_EVDO_0 EVDO网络, revision 0. 5 
	 * 
	 * NETWORK_TYPE_EVDO_A EVDO网络, revision A. 6 
	 * 
	 * NETWORK_TYPE_1xRTT 1xRTT网络 7 
	 */
	public static String getNetworkType(Context context) {
		try {
			NetworkInfo networkinfo = ((ConnectivityManager) context.getSystemService("connectivity"))
					.getActiveNetworkInfo();
			if (networkinfo != null) {
				return networkinfo.getType() + "";
			}
		} catch (Exception ex) {
			LogUtil.d(TAG, "getNetworkType : " + ex.toString());
		}
		return "Not Found NetworkType";
	}

	public static long getAvailMemory(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo memoryInfo = new MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);
		return memoryInfo.availMem;
	}

	/**
	 * 获取手机号码
	 * 
	 * @return
	 */
	public static String getPhoneNumber(Context context) {
		try {
			// 获取手机号 MSISDN，很可能为空
			TelephonyManager teleMng = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			switch (teleMng.getSimState()) { // getSimState()取得sim的状态 有下面6中状态
			case TelephonyManager.SIM_STATE_ABSENT:
				return "No Phone Card";

			case TelephonyManager.SIM_STATE_UNKNOWN:
				return "Unkown State";

			case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
				return "Need NetworkPIN To Unlock";

			case TelephonyManager.SIM_STATE_PIN_REQUIRED:
				return "Need PIN To Unlock";

			case TelephonyManager.SIM_STATE_PUK_REQUIRED:
				return "Need PUK To Unlock";

			case TelephonyManager.SIM_STATE_READY:
				break;
			}
			return null == teleMng.getLine1Number() ? "Not Found PhoneNumber" : teleMng.getLine1Number();
		} catch (Exception ex) {
			ex.printStackTrace();
			LogUtil.d(TAG, "getPhoneNumber : " + ex.toString());
		}
		return "Not Found PhoneNumber";
	}

}
