package com.itop.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.http.conn.ConnectTimeoutException;

/**
 * 网络请求工具类
 * 
 * @author Li.Jin
 *
 */
public class HttpUtil {

	private static final int REQUEST_TIME_OUT = 8 * 1000;
	private static final int READ_TIME_OUT = 8 * 1000;

	public static String getConnection(String paramURL) throws Exception {

		URL localURL = null;
		HttpURLConnection urlConnection = null;
		BufferedReader reader = null;

		try {
			localURL = new URL(paramURL);

			if (localURL != null) {
				urlConnection = (HttpURLConnection) localURL.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setConnectTimeout(REQUEST_TIME_OUT);
				urlConnection.setReadTimeout(READ_TIME_OUT);
				// urlConnection.setDoInput(true);
				// urlConnection.setDoOutput(true);
				// urlConnection.setUseCaches(false);

				StringBuffer sb = new StringBuffer();
				if (null != urlConnection && urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String lines;
					while ((lines = reader.readLine()) != null) {
						sb.append(lines);
					}
					return sb.toString();
				}
			}
		} catch (ConnectTimeoutException e) {
			LogUtil.d("Send POST request exception： ConnectTimeoutException!");
			throw new Exception(e);
		} catch (MalformedURLException e) {
			LogUtil.d("Send POST request exception： MalformedURLException!");
			throw new Exception(e);
		} catch (IOException e) {
			LogUtil.d("Send POST request error：" + e.getMessage());
			throw new Exception(e);
		} catch (Exception e) {
			LogUtil.d("Send POST request error：" + e.getMessage());
			throw new Exception(e);
		} finally {
			localURL = null;
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					throw new Exception(e);
				}
			}

			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return null;
	}

	/**
	 * Post请求
	 * 
	 * @param url请求地址
	 * @param param
	 *            请求参数(key1=value1&key2=value2)
	 * @return
	 * @throws Exception
	 *             异常
	 */
	public static String postConnection(String url, String param) throws Exception {
		PrintWriter printWriter = null;
		BufferedReader bufferedReader = null;
		HttpURLConnection httpURLConnection = null;

		StringBuffer responseResult = new StringBuffer();

		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			httpURLConnection = (HttpURLConnection) realUrl.openConnection();
			httpURLConnection.setConnectTimeout(REQUEST_TIME_OUT);
			httpURLConnection.setReadTimeout(READ_TIME_OUT);
			// 设置通用的请求属性
			httpURLConnection.setRequestProperty("accept", "*/*");
			httpURLConnection.setRequestProperty("connection", "Keep-Alive");
			httpURLConnection.setRequestProperty("Content-Length", String.valueOf(param.length()));
			// 发送POST请求必须设置如下两行
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			printWriter = new PrintWriter(httpURLConnection.getOutputStream());
			// 发送请求参数
			printWriter.write(param);
			// flush输出流的缓冲
			printWriter.flush();
			// 根据ResponseCode判断连接是否成功
			int responseCode = httpURLConnection.getResponseCode();
			if (responseCode == httpURLConnection.HTTP_OK) {
				// 定义BufferedReader输入流来读取URL的ResponseData
				bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					responseResult.append(line);
				}
				return responseResult.toString();
			}
			return null;
		} catch (ConnectTimeoutException e) {
			LogUtil.d("Send POST request exception： ConnectTimeoutException!");
			throw new Exception(e);
		} catch (MalformedURLException e) {
			LogUtil.d("Send POST request exception： MalformedURLException!");
			throw new Exception(e);
		} catch (IOException e) {
			LogUtil.d("Send POST request error：" + e.getMessage());
			throw new Exception(e);
		} catch (Exception e) {
			LogUtil.d("Send POST request error：" + e.getMessage());
			throw new Exception(e);
		} finally {
			httpURLConnection.disconnect();
			try {
				if (printWriter != null) {
					printWriter.close();
				}
				if (bufferedReader != null) {
					bufferedReader.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static String getConnection(String strURL, String charset) throws Exception {
		HttpURLConnection connection;
		BufferedReader reader;
		InputStream inputStream;
		URL url = null;
		connection = null;
		reader = null;
		inputStream = null;
		String result = null;
		try {
			url = new URL(strURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("accept-encoding", "gzip,deflate");
			connection.setUseCaches(false);
			connection.setConnectTimeout(REQUEST_TIME_OUT);
			connection.setReadTimeout(READ_TIME_OUT);
			connection.setRequestProperty("Referer", strURL);
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0");
			inputStream = connection.getInputStream();
			String contentEncoding = connection.getContentEncoding();
			if (contentEncoding == null) {
				contentEncoding = charset;
			}
			String headerField = connection.getHeaderField("Content-Type");
			if (headerField.contains("charset")) {
				String content_types[] = headerField.split("\\;");
				for (int i = 0; i < content_types.length; i++) {
					if (!content_types[i].contains("charset"))
						continue;
					charset = content_types[i].split("\\=")[1];
					break;
				}
			}
			if (contentEncoding.contains("gzip")) {
				reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(inputStream), charset));
			} else {
				reader = new BufferedReader(new InputStreamReader(inputStream, charset));
			}
			StringBuffer buffer = new StringBuffer();
			for (String line = reader.readLine(); line != null;) {
				buffer.append(line);
				try {
					line = reader.readLine();
				} catch (Exception e) {
					line = null;
				}
			}
			result = buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.d("getResult request error：" + e.getMessage());
		}
		return result;
	}
}
