package com.itop.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class ThreadSafeDateFormat extends ThreadLocal<DateFormat> {

    private String pattern;
    private Locale locale;
    private TimeZone timezone;

    public ThreadSafeDateFormat(String pattern) {
        this(pattern, Locale.US);
    }

    public ThreadSafeDateFormat(String pattern, Locale locale) {
        this.pattern = pattern;
        this.locale = locale;
    }

    public void setTimeZone(TimeZone timezone) {
        this.timezone = timezone;
    }

    @Override
    protected DateFormat initialValue() {
        SimpleDateFormat df = new SimpleDateFormat(pattern, locale);
        if (timezone != null) {
            df.setTimeZone(timezone);
        }
        return df;
    }

    public Date parse(String dateString) throws ParseException {
        return get().parse(dateString);
    }

    public String format(Date date) {
        return get().format(date);
    }
}
