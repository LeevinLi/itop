/**
 * 
 */
package com.itop.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with eclipse Leevin 2014年6月10日 下午6:05:27
 */
public class DateUtil {

	private static final String TAG = DateUtil.class.getSimpleName();

	public static final ThreadSafeDateFormat FORMAT_DETAIL = new ThreadSafeDateFormat("yyyy-MM-dd HH:mm:ssZ");
	public static final ThreadSafeDateFormat FORMAT_DATE = new ThreadSafeDateFormat("yyyy-MM-dd HH:mm");
	public static final ThreadSafeDateFormat FORMAT_ONLY_MONTH = new ThreadSafeDateFormat("MM-dd");
	public static final ThreadSafeDateFormat FORMAT_MONTH = new ThreadSafeDateFormat("MM-dd HH:mm");
	public static final ThreadSafeDateFormat FORMAT_TIME = new ThreadSafeDateFormat("HH:mm");
	public static final ThreadSafeDateFormat FORMAT_DAY_OF_MONTH = new ThreadSafeDateFormat("yyyy-MM-dd");

	public static String dateFormat(String format, int after) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf;
		if (after != 0) {
			cal.add(5, after);
			sdf = new SimpleDateFormat(format);
			return sdf.format(cal.getTime());
		}
		return "1970年00月00日";
	}

	public static String week(int after) {
		String str = "";
		Calendar cal = Calendar.getInstance();
		try {
			if (after != 0) {
				cal.add(7, after);
			}
			int idx = cal.get(7);
			switch (idx) {
			case 7: // '\007'
				str = "星期六";
				break;

			case 1: // '\001'
				str = "星期日";
				break;

			case 2: // '\002'
				str = "星期一";
				break;

			case 3: // '\003'
				str = "星期二";
				break;

			case 4: // '\004'
				str = "星期三";
				break;

			case 5: // '\005'
				str = "星期四";
				break;

			case 6: // '\006'
				str = "星期五";
				break;
			}
		} catch (Exception e) {
			LogUtil.d(TAG, e.getMessage());
		}
		return str;
	}

}
