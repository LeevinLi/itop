package com.itop.utils;

import org.apache.http.protocol.HTTP;

/**
 * iTop框架所有常量
 * 
 * @author Jin.Li
 * @date 2014年10月11日
 */
public class iTopConstants {
	public static final String TAG = "iTop";
	public static final String UTF_8 = HTTP.UTF_8;

	// DataFormat
	public static final ThreadSafeDateFormat FORMAT_DETAIL = new ThreadSafeDateFormat("yyyy-MM-dd HH:mm:ssZ");
	public static final ThreadSafeDateFormat FORMAT_DATE = new ThreadSafeDateFormat("yyyy-MM-dd HH:mm");
	public static final ThreadSafeDateFormat FORMAT_ONLY_MONTH = new ThreadSafeDateFormat("MM-dd");
	public static final ThreadSafeDateFormat FORMAT_MONTH = new ThreadSafeDateFormat("MM-dd HH:mm");
	public static final ThreadSafeDateFormat FORMAT_TIME = new ThreadSafeDateFormat("HH:mm");
	public static final ThreadSafeDateFormat FORMAT_DAY_OF_MONTH = new ThreadSafeDateFormat("yyyy-MM-dd");

}
