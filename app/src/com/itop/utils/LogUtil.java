package com.itop.utils;

import android.util.Log;

/**
 * Created with Intellij IDEA User: Leevin Date: on 2014/7/4 0004. Time: 15:04.
 */
public class LogUtil {

	private static final int DEBUG = Log.DEBUG;

	public static void v(String tag, String message) {
		if (DEBUG >= Log.VERBOSE) {
			Log.v(tag, message);
		}
	}

	public static void v(String message) {
		if (DEBUG >= Log.VERBOSE) {
			Log.v(iTopConstants.TAG, message);
		}
	}

	public static void d(String message) {
		if (DEBUG <= Log.DEBUG) {
			Log.d(iTopConstants.TAG, message);
		}
	}

	public static void d(String tag, String message) {
		if (DEBUG <= Log.DEBUG) {
			Log.d(tag, message);
		}
	}

	public static void i(String message) {
		if (DEBUG <= Log.INFO) {
			Log.i(iTopConstants.TAG, message);
		}
	}

	public static void i(String tag, String message) {
		if (DEBUG <= Log.INFO) {
			Log.i(tag, message);
		}
	}

	public static void w(String message) {
		if (DEBUG <= Log.WARN) {
			Log.w(iTopConstants.TAG, message);
		}
	}

	public static void w(String tag, String message) {
		if (DEBUG <= Log.WARN) {
			Log.w(tag, message);
		}
	}

	public static void e(String tag, String message) {
		if (DEBUG <= Log.ERROR) {
			Log.e(tag, message);
		}
	}

	public static void e(String message) {
		if (DEBUG <= Log.ERROR) {
			Log.e(iTopConstants.TAG, message);
		}
	}
}
