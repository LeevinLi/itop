package com.itop.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;

/**
 * 文件工具类
 * 
 * @author Jin.Li
 * @date 2014年10月11日
 */
public class FileUtil {

	private static final String TAG = FileUtil.class.getSimpleName();

	/**
	 * 创建新文件夹
	 * 
	 * @param dir
	 * @return
	 */
	public static boolean createDir(String dir) {
		File file = new File(dir);
		if (!file.exists()) {
			return file.mkdirs();
		}
		return true;
	}

	/**
	 * 创建新文件
	 * 
	 * @param dir
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public static boolean createFile(String dir, String name) throws IOException {
		File file = new File(dir, name);
		if (!file.exists()) {
			return file.createNewFile();
		}
		return true;
	}

	public static InputStream openFile(Context context, String fileName) {
		try {
			File file = context.getFileStreamPath(fileName);
			if (file.exists()) {
				return new FileInputStream(file);
			}
		} catch (IOException e) {
			LogUtil.d(TAG, "openFile : " + e.getMessage());
		}
		return null;
	}

	public static File getCacheFile(Context context, String fileName) {
		File externalDir = context.getExternalCacheDir();
		if (externalDir == null || !externalDir.canRead() || !externalDir.canWrite()) {
			return null;
		}
		return new File(externalDir, fileName);
	}

	public static File getExternalFile(Context context, String type, String name) {
		File externalDir = context.getExternalFilesDir(type);
		if (externalDir == null || !externalDir.canRead() || !externalDir.canWrite()) {
			return null;
		}
		return new File(externalDir, name);
	}

	/**
	 * 
	 * @param context
	 * @param type
	 * @param id
	 * @param ext
	 * @return
	 */
	public static File getExternalFile(Context context, String type, int id, String ext) {
		File dir = getExternalFile(context, type, String.valueOf(id / 100));
		if (dir == null || (!dir.exists() && !dir.mkdirs())) {
			return null;
		}
		return new File(dir, id + ext);
	}

	/**
	 * 删除文件
	 * 
	 * @param file
	 * @return
	 */
	public static boolean deleteQuietly(File file) {
		if (file != null && file.exists()) {
			if (!file.delete()) {
				LogUtil.d(TAG, "Delete file failed : " + file.getAbsolutePath());
				return false;
			}
		}
		return true;
	}

	/**
	 * 保存数据,保存位置为当前应用私有的存储空间,文件目录：data/data/包名/files/
	 * 
	 * @param fileName
	 *            文件名称
	 * @param content
	 *            内容
	 */
	public static void saveFile(Context context, String fileName, String content) throws IOException {
		FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		fos.write(content.getBytes());
		fos.close();
		LogUtil.d(TAG, "saveFile :  " + fileName);
	}

	/**
	 * 保存二进制数据,保存位置为当前应用私有的存储空间,文件目录：data/data/包名/files/
	 * 
	 * @param fileName
	 *            文件名称
	 * @param content
	 *            内容
	 */
	public static void saveFile(Context context, String fileName, InputStream content) throws IOException {
		FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		BufferedOutputStream buf = new BufferedOutputStream(fos);
		byte[] buffer = new byte[1024];
		int length;
		try {
			while ((length = content.read(buffer)) != -1) {
				buf.write(buffer, 0, length);
			}
		} catch (Exception e) {
			LogUtil.d(TAG, "saveFile : " + e.getMessage());
		} finally {
			buf.flush();
			buf.close();
			fos.close();
		}
	}

	/**
	 * 根据文件名，获取此文件内容的字节数组,文件目录：data/data/包名/files/
	 * 
	 * @param fileName
	 *            有效的文件名
	 * @return 文件内容的字节数组
	 * @throws Exception
	 */
	public static byte[] readFile(Context context, String fileName) throws IOException {
		FileInputStream fis = context.openFileInput(fileName);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		while ((length = fis.read(buffer)) != -1) {
			bos.write(buffer, 0, length);
		}
		fis.close();
		bos.close();
		return bos.toByteArray();
	}
}
